galacteek.ld
============

This repository contains the JSON-LD schemas for
the *galacteek.ld* IPS catalog (IRI: *ips://galacteek.ld/*).

Building
--------

This will transform the contexts written in *yaml-ld* into
*json-ld*:

```sh
python setup.py build
```

Contributing
------------

If you write a new schema, please use the *yaml-ld* format
as it is a bit easier to maintain.
