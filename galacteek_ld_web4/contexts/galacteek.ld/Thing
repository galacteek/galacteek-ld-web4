{
    "@context": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "schema": "ips://galacteek.ld/",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@graph": [
        {
            "@id": "schema:url",
            "@type": "rdf:Property",
            "rdfs:comment": "URL of the item.",
            "rdfs:label": "url",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:sameAs",
            "@type": "rdf:Property",
            "rdfs:comment": "URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Wikidata entry, or official website.",
            "rdfs:label": "sameAs",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:additionalType",
            "@type": "rdf:Property",
            "rdfs:comment": "An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.",
            "rdfs:label": "additionalType",
            "rdfs:subPropertyOf": {
                "@id": "rdf:type"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:disambiguatingDescription",
            "@type": "rdf:Property",
            "rdfs:comment": "A sub property of description. A short description of the item used to disambiguate from other, similar items. Information from other properties (in particular, name) may be necessary for the description to be useful for disambiguation.",
            "rdfs:label": "disambiguatingDescription",
            "rdfs:subPropertyOf": {
                "@id": "schema:description"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:Thing",
            "@type": "rdfs:Class",
            "rdfs:comment": "The most generic type of item.",
            "rdfs:label": "Thing"
        },
        {
            "@id": "schema:potentialAction",
            "@type": "rdf:Property",
            "rdfs:comment": "Indicates a potential Action, which describes an idealized action in which this thing would play an 'object' role.",
            "rdfs:label": "potentialAction",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Action"
            }
        },
        {
            "@id": "schema:description",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/description"
            },
            "rdfs:comment": "A description of the item.",
            "rdfs:label": "description",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:image",
            "@type": "rdf:Property",
            "rdfs:comment": "An image of the item. This can be a [[URL]] or a fully described [[ImageObject]].",
            "rdfs:label": "image",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:ImageObject"
                },
                {
                    "@id": "schema:URL"
                }
            ]
        },
        {
            "@id": "schema:alternateName",
            "@type": "rdf:Property",
            "rdfs:comment": "An alias for the item.",
            "rdfs:label": "alternateName",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:name",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/title"
            },
            "rdfs:comment": "The name of the item.",
            "rdfs:label": "name",
            "rdfs:subPropertyOf": {
                "@id": "rdfs:label"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:identifier",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/identifier"
            },
            "rdfs:comment": "The identifier property represents any kind of identifier for any kind of [[Thing]], such as ISBNs, GTIN codes, UUIDs etc. Schema.org provides dedicated properties for representing many of these, either as textual strings or as URL (URI) links. See [background notes](/docs/datamodel.html#identifierBg) for more details.\n        ",
            "rdfs:label": "identifier",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:PropertyValue"
                },
                {
                    "@id": "schema:URL"
                },
                {
                    "@id": "schema:Text"
                }
            ]
        },
        {
            "@id": "schema:subjectOf",
            "@type": "rdf:Property",
            "rdfs:comment": "A CreativeWork or Event about this Thing.",
            "rdfs:label": "subjectOf",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:inverseOf": {
                "@id": "schema:about"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:CreativeWork"
                },
                {
                    "@id": "schema:Event"
                }
            ],
            "schema:source": {
                "@id": "https://github.com/schemaorg/schemaorg/issues/1670"
            }
        },
        {
            "@id": "schema:mainEntityOfPage",
            "@type": "rdf:Property",
            "rdfs:comment": "Indicates a page (or other CreativeWork) for which this thing is the main entity being described. See [background notes](/docs/datamodel.html#mainEntityBackground) for details.",
            "rdfs:label": "mainEntityOfPage",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:inverseOf": {
                "@id": "schema:mainEntity"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:URL"
                },
                {
                    "@id": "schema:CreativeWork"
                }
            ]
        }
    ]
}
