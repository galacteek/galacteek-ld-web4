{
    "@context": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "schema": "ips://galacteek.ld/",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@graph": [
        {
            "@id": "schema:sameAs",
            "@type": "rdf:Property",
            "rdfs:label": "sameAs",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:ReactAction",
            "@type": "rdfs:Class",
            "rdfs:label": "ReactAction",
            "rdfs:subClassOf": {
                "@id": "schema:AssessAction"
            }
        },
        {
            "@id": "schema:AssessAction",
            "@type": "rdfs:Class",
            "rdfs:label": "AssessAction",
            "rdfs:subClassOf": {
                "@id": "schema:Action"
            }
        },
        {
            "@id": "schema:Thing",
            "@type": "rdfs:Class",
            "rdfs:label": "Thing"
        },
        {
            "@id": "schema:DisagreeAction",
            "@type": "rdfs:Class",
            "rdfs:label": "DisagreeAction",
            "rdfs:subClassOf": {
                "@id": "schema:ReactAction"
            }
        },
        {
            "@id": "schema:Action",
            "@type": "rdfs:Class",
            "rdfs:label": "Action",
            "rdfs:subClassOf": {
                "@id": "schema:Thing"
            },
            "schema:source": {
                "@id": "http://www.w3.org/wiki/WebSchemas/SchemaDotOrgSources#source_ActionCollabClass"
            }
        },
        {
            "@id": "schema:description",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/description"
            },
            "rdfs:label": "description",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:object",
            "@type": "rdf:Property",
            "rdfs:label": "object",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Thing"
            }
        },
        {
            "@id": "schema:actionStatus",
            "@type": "rdf:Property",
            "rdfs:label": "actionStatus",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:ActionStatusType"
            }
        },
        {
            "@id": "schema:potentialAction",
            "@type": "rdf:Property",
            "rdfs:label": "potentialAction",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Action"
            }
        },
        {
            "@id": "schema:image",
            "@type": "rdf:Property",
            "rdfs:label": "image",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:URL"
                },
                {
                    "@id": "schema:ImageObject"
                }
            ]
        },
        {
            "@id": "schema:agent",
            "@type": "rdf:Property",
            "rdfs:label": "agent",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:Organization"
                },
                {
                    "@id": "schema:Person"
                }
            ]
        },
        {
            "@id": "schema:target",
            "@type": "rdf:Property",
            "rdfs:label": "target",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:EntryPoint"
            }
        },
        {
            "@id": "schema:identifier",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/identifier"
            },
            "rdfs:label": "identifier",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:URL"
                },
                {
                    "@id": "schema:PropertyValue"
                },
                {
                    "@id": "schema:Text"
                }
            ]
        },
        {
            "@id": "schema:subjectOf",
            "@type": "rdf:Property",
            "rdfs:label": "subjectOf",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:inverseOf": {
                "@id": "schema:about"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:CreativeWork"
                },
                {
                    "@id": "schema:Event"
                }
            ],
            "schema:source": {
                "@id": "https://github.com/schemaorg/schemaorg/issues/1670"
            }
        },
        {
            "@id": "schema:mainEntityOfPage",
            "@type": "rdf:Property",
            "rdfs:label": "mainEntityOfPage",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:inverseOf": {
                "@id": "schema:mainEntity"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:URL"
                },
                {
                    "@id": "schema:CreativeWork"
                }
            ]
        },
        {
            "@id": "schema:additionalType",
            "@type": "rdf:Property",
            "rdfs:label": "additionalType",
            "rdfs:subPropertyOf": {
                "@id": "rdf:type"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:disambiguatingDescription",
            "@type": "rdf:Property",
            "rdfs:label": "disambiguatingDescription",
            "rdfs:subPropertyOf": {
                "@id": "schema:description"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:error",
            "@type": "rdf:Property",
            "rdfs:label": "error",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Thing"
            }
        },
        {
            "@id": "schema:alternateName",
            "@type": "rdf:Property",
            "rdfs:label": "alternateName",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:startTime",
            "@type": "rdf:Property",
            "rdfs:label": "startTime",
            "schema:domainIncludes": [
                {
                    "@id": "schema:Schedule"
                },
                {
                    "@id": "schema:MediaObject"
                },
                {
                    "@id": "schema:FoodEstablishmentReservation"
                },
                {
                    "@id": "schema:Action"
                }
            ],
            "schema:rangeIncludes": [
                {
                    "@id": "schema:DateTime"
                },
                {
                    "@id": "schema:Time"
                }
            ],
            "schema:source": {
                "@id": "https://github.com/schemaorg/schemaorg/issues/2493"
            }
        },
        {
            "@id": "schema:name",
            "@type": "rdf:Property",
            "http://www.w3.org/2002/07/owl#equivalentProperty": {
                "@id": "http://purl.org/dc/terms/title"
            },
            "rdfs:label": "name",
            "rdfs:subPropertyOf": {
                "@id": "rdfs:label"
            },
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Text"
            }
        },
        {
            "@id": "schema:url",
            "@type": "rdf:Property",
            "rdfs:label": "url",
            "schema:domainIncludes": {
                "@id": "schema:Thing"
            },
            "schema:rangeIncludes": {
                "@id": "schema:URL"
            }
        },
        {
            "@id": "schema:instrument",
            "@type": "rdf:Property",
            "rdfs:label": "instrument",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Thing"
            }
        },
        {
            "@id": "schema:endTime",
            "@type": "rdf:Property",
            "rdfs:label": "endTime",
            "schema:domainIncludes": [
                {
                    "@id": "schema:MediaObject"
                },
                {
                    "@id": "schema:FoodEstablishmentReservation"
                },
                {
                    "@id": "schema:Action"
                },
                {
                    "@id": "schema:Schedule"
                }
            ],
            "schema:rangeIncludes": [
                {
                    "@id": "schema:DateTime"
                },
                {
                    "@id": "schema:Time"
                }
            ],
            "schema:source": {
                "@id": "https://github.com/schemaorg/schemaorg/issues/2493"
            }
        },
        {
            "@id": "schema:participant",
            "@type": "rdf:Property",
            "rdfs:label": "participant",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": [
                {
                    "@id": "schema:Organization"
                },
                {
                    "@id": "schema:Person"
                }
            ]
        },
        {
            "@id": "schema:result",
            "@type": "rdf:Property",
            "rdfs:label": "result",
            "schema:domainIncludes": {
                "@id": "schema:Action"
            },
            "schema:rangeIncludes": {
                "@id": "schema:Thing"
            }
        },
        {
            "@id": "schema:location",
            "@type": "rdf:Property",
            "rdfs:label": "location",
            "schema:domainIncludes": [
                {
                    "@id": "schema:Action"
                },
                {
                    "@id": "schema:Organization"
                },
                {
                    "@id": "schema:Event"
                }
            ],
            "schema:rangeIncludes": [
                {
                    "@id": "schema:Text"
                },
                {
                    "@id": "schema:Place"
                },
                {
                    "@id": "schema:VirtualLocation"
                },
                {
                    "@id": "schema:PostalAddress"
                }
            ]
        }
    ]
}