import json
import traceback
from pathlib import Path

import yaml
from yaml import Loader

extension = '.yaml-ld'

default_ns = {
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "gs": "ips://galacteek.ld/",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
}


def yaml_ld_to_json(tmpl: Path, dst: Path = None):
    if not tmpl.name.endswith(extension):
        return None

    dst = str(dst) if dst else str(tmpl).replace(extension, '')
    try:
        with open(str(tmpl), 'rt', encoding='utf8') as yfd:
            doc = yaml.load(yfd, Loader=Loader)

        doc['@context']['@version'] = 1.1
        doc['@context'].update(**default_ns)

        with open(dst, 'w+t') as fd:
            fd.write(json.dumps(doc, indent=4))

        fd.close()
    except Exception:
        traceback.print_exc()
        return False
    else:
        return True
