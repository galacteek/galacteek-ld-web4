import sys
import json
import pkg_resources
from pathlib import Path

from setuptools import setup
from setuptools import Command
from setuptools import find_packages
from distutils.command.build import build

from galacteek_ld_web4.__version__ import __version__


PY_VER = sys.version_info

if PY_VER >= (3, 6):
    pass
else:
    print('You need python3.6 or newer')
    print('Your python version is {0}'.format(PY_VER))
    raise RuntimeError('Invalid python version')


class build_contexts(Command):
    user_options = []

    def initialize_options(self):
        contextsRoot = pkg_resources.resource_filename(
            'galacteek_ld_web4.contexts', ''
        )

        self.ctxs = []

        for src in ['galacteek.ld',
                    'galacteek.ld/ui',
                    'galacteek.ld/ipfs']:
            p = Path(contextsRoot).joinpath(src)
            if p.is_dir():
                self.ctxs += list(p.glob('*'))

    def finalize_options(self):
        pass

    def run(self):
        from galacteek_ld_web4.yaml_ld_converter import yaml_ld_to_json

        for ctx in self.ctxs:
            if ctx.is_symlink():
                continue

            if not ctx.is_file():
                continue
            if ctx.name.startswith('.') or ctx.name.endswith('.py'):
                continue

            if ctx.name.endswith('.yaml-ld'):
                assert yaml_ld_to_json(ctx) is True
                continue

            try:
                with open(str(ctx), 'rt') as fd:
                    obj = json.load(fd)
                    assert obj is not None
                    assert isinstance(obj, dict) is True
                    assert '@context' in obj
            except Exception:
                print(f'{ctx}: INVALID')
            else:
                print(f'{ctx}: OK')


class _build(build):
    sub_commands = [('build_contexts', None)] + build.sub_commands


with open('README.md', 'r') as fh:
    long_description = fh.read()

deps_links = []


def reqs_parse(path):
    reqs = []
    deps = []

    with open(path) as f:
        lines = f.read().splitlines()
        for line in lines:
            if line.startswith('-e'):
                link = line.split().pop()
                deps.append(link)
            else:
                reqs.append(line)

    return reqs


install_reqs = reqs_parse('requirements.txt')


found_packages = find_packages(exclude=['tests', 'tests.*'])

setup(
    name='galacteek-ld-web4',
    version=__version__,
    license='MIT',
    author='cipres',
    url='https://gitlab.com/galacteek/galacteek-ld-web4',
    description='Galacteek web4 LD contexts',
    long_description=long_description,
    cmdclass={
        'build_contexts': build_contexts,
        'build': _build
    },
    packages=found_packages,
    install_requires=install_reqs,
    dependency_links=deps_links,
    classifiers=[
        'Environment :: X11 Applications :: Qt',
        'Framework :: AsyncIO',
        'Topic :: Desktop Environment :: File Managers',
        'Topic :: Internet :: WWW/HTTP :: Browsers',
        'Development Status :: 4 - Beta',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Topic :: System :: Filesystems',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9'
    ],
    keywords=[
        'dweb',
        'ipfs',
        'json-ld'
    ]
)
